const gulp = require('gulp');
const browserSync = require('browser-sync');
const injector = require('bs-html-injector');
const reload = browserSync.reload;
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const prettyHtml = require('gulp-pretty-html');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const lost = require('lost');
const svgo = require('gulp-svgo');
const watch = require('gulp-watch');
const webpack = require('webpack-stream');


const clean = require('gulp-clean');
const csscomb = require('gulp-csscomb');
const runSequence = require('run-sequence');
const csso = require('gulp-csso');

const path = {
  build: {
    html: './build/',
    js: './build/js/',
    css: './build/css/',
    files: './build/files/',
    images: './build/images/',
    fonts: './build/fonts/'
  },
  src: {
    html: './src/pug/pages/*.pug',
    js: './src/js/**/*.js',
    sass: './src/sass/**/*.scss',
    files: './src/files/**/*',
    images: './src/images/**/*',
    fonts: './src/fonts/**'
  },
  watch: {
    html: './src/pug/**/*.pug',
    js: './src/js/**/*.js',
    sass: './src/sass/**/*.scss',
    files: './src/files/**/*',
    images: './src/images/**/*',
    fonts: './src/fonts/**/*.*'
  },
  clean: './build'
};


gulp.task('browserSync', function () {
  browserSync.use(injector);
  browserSync({
    server: { baseDir: path.build.html },
    notify: false,
    open: false,
    ghostMode: false
  });
});

gulp.task('browserSync_drupal', function () {
  browserSync.use(injector);
  browserSync({
    // server: { baseDir: path.build.html },
    proxy: 'premo-19.localhost',
    notify: false,
    open: false,
    ghostMode: false
  });
});

gulp.task('js', function () {
  return gulp.src('./src/js/main.js')
    .pipe(webpack(require('./webpack.config')))
    .pipe(plumber())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({ stream: true }));
});

gulp.task('sass', function () {
  gulp.src([path.src.sass])
    .pipe(sourcemaps.init({ loadMap: true }))
    .pipe(sass({ outputStyle: 'compressed' })).on('error', sass.logError)
    .pipe(postcss([
      lost()
    ]))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    // .pipe(concat('styles.css'))
    .pipe(csso())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({ stream: true }));
});
gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.html)
    .pipe(plumber())
    .pipe(pug())
    .pipe(prettyHtml({
      indent_size: 2,
      brace_style: 'expand',
      inline: ['']
    }))
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({ stream: true }));
});
gulp.task('files', function () {
  return gulp.src([path.src.files, '!README.md'])
    .pipe(svgo())
    .pipe(gulp.dest(path.build.files))
    .pipe(reload({ stream: true }));
});
gulp.task('images', function () {
  return gulp.src([path.src.images, '!README.md'])
    .pipe(svgo())
    .pipe(gulp.dest(path.build.images))
    .pipe(reload({ stream: true }));
});
gulp.task('fonts', function () {
  return gulp.src([path.src.fonts, '!README.md'])
    .pipe(gulp.dest(path.build.fonts))
});
gulp.task('root-copy', function () {
  return gulp.src([
    './src/*.png',
    './src/browserconfig.xml',
    './src/favicon.ico',
    './src/manifest.json',
  ])
    .pipe(gulp.dest('./build/'))
});

gulp.task('ss', function () {
  return gulp.src(path.src.sass)
    .pipe(csscomb())
    .pipe(gulp.dest('./src/sass/'))
});

// gulp.task('critical', function () {
//   critical.generate({
//     inline: true,
//     base: './build/',
//     src: 'index.html',
//     css: './build/css/styles.css',
//     // extract: true,
//     dest: 'index.html',
//     minify: true,
//     width: 1920,
//     height: 930
//   });
// });


gulp.task('build', function () {
  runSequence('clean', 'sass', 'pug', 'files', 'images', 'fonts', 'js', 'root-copy');
});
gulp.task('clean', function () {
  return gulp.src('./build', { read: false })
    .pipe(clean());
});


gulp.task('watch', function () {
  watch(path.watch.sass, function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 100);
  });
  watch(path.watch.html, function () {
    setTimeout(function () {
      gulp.start('pug');
    }, 100);
  });
  watch(path.watch.js, function () {
    setTimeout(function () {
      // gulp.start('js');
      reload({ stream: true });
    }, 100);
  });
  watch(path.watch.files, function () {
    setTimeout(function () {
      gulp.start('files');
    }, 100);
  });
  watch(path.watch.images, function () {
    setTimeout(function () {
      gulp.start('images');
    }, 100);
  });
  watch(path.watch.fonts, function () {
    setTimeout(function () {
      gulp.start('fonts');
    }, 100);
  });
});
gulp.task('watch_drupal', function () {
  watch(path.watch.sass, function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 100);
  });
  // watch(path.watch.html, function () {
  //   setTimeout(function () {
  //     gulp.start('pug');
  //   }, 100);
  // });
  watch(path.watch.js, function () {
    setTimeout(function () {
      // gulp.start('js');
      reload({ stream: true });
    }, 100);
  });
  // watch(path.watch.files, function () {
  //   setTimeout(function () {
  //     gulp.start('files');
  //   }, 100);
  // });
  watch(path.watch.images, function () {
    setTimeout(function () {
      gulp.start('images');
    }, 100);
  });
  watch(path.watch.fonts, function () {
    setTimeout(function () {
      gulp.start('fonts');
    }, 100);
  });
});
gulp.task('default', ['build', 'browserSync', 'watch']);
gulp.task('drupal', ['build', 'browserSync_drupal', 'watch_drupal']);

#!/bin/bash

IN_DIR='./src/images/pp/'
OUT_DIR='./src/files/dev-preview/'

rm $OUT_DIR*.jpg


for file in `ls -1c $IN_DIR*.png`
do
    filename=${file%.*}
    filename=${filename##*/}'.jpg'

    if convert $file -resize 400x $OUT_DIR$filename
    then echo $file'.jpg'
    else echo 'Error: '$file'.jpg'
    fi


done

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.tags_scroll = {
    attach: function (context, settings) {

      function TagsScroll(node) {
        this.node = node;
        this.prev = node.querySelector('.tagList__arrow--prev');
        this.next = node.querySelector('.tagList__arrow--next');
        this.scroll = node.querySelector('.tagList__scroll');
        this.offset = null;
        this.step = 300;
        this.speed = 0.2;
        this.duration = null;
        this.tl = null;
        this.paused = false;

        this.reverse = this.reverse.bind(this);
        this.start = this.start.bind(this);

        this.init();
      }

      TagsScroll.prototype.init = function () {
        this.next.addEventListener('click', function (e) {
          e.preventDefault();
          this.moveBy(1);
        }.bind(this));
        this.prev.addEventListener('click', function (e) {
          e.preventDefault();
          this.moveBy(-1);
        }.bind(this));

        this.offset = this.getOffset();
        this.duration = this.getDuration();


        this.initMoving();
        this.start();
      };

      TagsScroll.prototype.moveBy = function (value) {
        var scroll = this.scroll;
        var offset = this.scroll.scrollLeft + value * this.step;
        var duration = this.speed;

        var curPos = this.tl.totalProgress();
        var newPos = (curPos + 10/this.duration * value);
        newPos = newPos < 0 ? 0 : newPos;

        TweenLite.to(this.tl, duration, { progress: newPos});
      };

      TagsScroll.prototype.initMoving = function () {
        var scroll = this.scroll;
        var duration = this.duration;
        var offset = this.offset;

        this.tl = new TimelineLite({
          onComplete: this.reverse,
          onReverseComplete: this.start,
          paused: true
        });
        this.tl.to(scroll, duration, {scrollTo: {y: 0, x: offset}, ease: Power0.easeNone});

        this.node.addEventListener('mouseenter', function (e) {
          this.paused = true;
          this.tl.pause();
        }.bind(this));
        this.node.addEventListener('mouseleave', function (e) {
          this.paused = false;
          this.tl.resume();
        }.bind(this));
      };

      TagsScroll.prototype.start = function () {
        this.tl.totalDuration(this.duration);
        !this.paused ? this.tl.restart() : null;
      };

      TagsScroll.prototype.reverse = function () {
        this.tl
          .totalDuration(1)
          .reverse();
      };

      TagsScroll.prototype.getDuration = function () {
        var stepPx = this.step;
        var width = this.offset;

        return (width/stepPx) * 4;
      };

      TagsScroll.prototype.getOffset = function () {
        var nodeW = this.scroll.offsetWidth;
        var scrollWidth = this.scroll.scrollWidth;

        return scrollWidth - nodeW;
      };

      var node = document.querySelector('.tagList');
      new TagsScroll(node);

    }
  };

})(jQuery, Drupal, drupalSettings);

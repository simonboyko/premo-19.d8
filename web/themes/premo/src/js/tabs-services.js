const tab = Tabs['services'];

try {
  if (tab) {
    const d = document;
    let text = '';
    const textEl = d.querySelector('.b-services__decorText');

    tab.on('update', updateHandler);
    function updateHandler(e) {
      console.log(e.detail);
      text = '';

      for(let i = 0; i < 7; i++) {
        text = `${text} ${e.detail.text}`;
      }
      fireAnim();
    }

    function fireAnim() {
      const animClass = '-fireAnim';
      const duration = 500;

      const start = () => {
        if (NODE_ENV === 'development') {
          console.log('start');
        }
        textEl.classList.add(animClass);
        setTimeout(() => {
          textEl.classList.remove(animClass);
          textEl.innerHTML = text;
          textEl.setAttribute('data-text', text);
        }, duration);
      };

      start();
    }
  }
} catch (e) {
  console.log(e);
}



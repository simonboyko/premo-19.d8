import { tns } from "../../node_modules/tiny-slider/src/tiny-slider";
import tween from 'TweenLite';
import TimelineLite from 'TimelineLite';


export default ((d) => {
  if (!d.querySelector('.mainSlider')) return;

  if (NODE_ENV === 'development') {
    console.log('INIT main-slider');
  }

  const total = d.querySelector('.mainSlider__total');
  const current = d.querySelector('.mainSlider__current');

  const initHandler = (e) => {
    total.innerHTML = e.slideCount;
    current.innerHTML = '1';
  };


  const slider = tns({
    container: d.querySelector('.mainSlider__slides'),
    nav: false,
    prevButton: d.querySelector('.mainSlider__arrow--prev'),
    nextButton: d.querySelector('.mainSlider__arrow--next'),
    speed: 500,
    loop: false,
    onInit: initHandler,
  });


  // const cover1 = document.querySelector('.mainSlider__cover--1');
  // const cover2 = document.querySelector('.mainSlider__cover--2');

  const changeHandler = (e) => {
    current.innerHTML = e.displayIndex;
    const direction = (e.displayIndex - e.indexCached) > 0 ? 'forward' : 'backward';

    // if (direction === 'forward') {
    //   const tlForward = new TimelineLite();
    //   tlForward
    //     .to(cover1, 0, {left: 0, right: '100%'})
    //     .to(cover2, 0, {left: 0, right: '100%'})
    //     .to(cover1, 0.6, { right: 0, ease: Power1.easeOut })
    //     .to(cover2, 0.6, { right: 0, ease: Power1.easeOut }, 0.2)
    //     .to(cover1, 1, { left: '100%', ease: Expo.easeOut }, 1)
    //     .to(cover2, 1, { left: '100%', ease: Expo.easeOut }, 1);
    // } else if (direction === 'backward') {
    //   const tlBackward = new TimelineLite();
    //   tlBackward
    //     .to(cover1, 0, {left: '100%', right: 0})
    //     .to(cover2, 0, {left: '100%', right: 0})
    //     .to(cover1, 0.6, { left: 0, ease: Power1.easeOut })
    //     .to(cover2, 0.6, { left: 0, ease: Power1.easeOut }, 0.2)
    //     .to(cover1, 1, { right: '100%', ease: Expo.easeOut }, 1)
    //     .to(cover2, 1, { right: '100%', ease: Expo.easeOut }, 1);
    // }

    console.log(e.slideItems[e.displayIndex]);
    const sliderInner = e.slideItems[e.displayIndex - 1].querySelector('.projectMainslider__inner');
    const tlSlider = new TimelineLite();
    tlSlider
      .to(sliderInner, 0, {opacity: 0, y: 100})
      .to(sliderInner, 1, {opacity: 1, y: 0}, 0.7);
  };
  slider.events.on('indexChanged', changeHandler);
})(document);



import CrawlLine from './crawl-line';

const node = document.querySelector('.b-clients__scroll');
if (node) {
  new CrawlLine(node, 'b-clients');
}

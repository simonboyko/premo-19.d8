class Faq {
  constructor(node) {
    this.node = node;
    this.title = node.querySelector('.faq__title');
    this.body = node.querySelector('.faq__content');
    this.closeH = null;
    this.openH = null;
    this.opened = false;

    this.clickHandler = this.clickHandler.bind(this);

    window.addEventListener('load', this.init.bind(this));
  }

  init() {
    this.closeH = this.node.offsetHeight;
    this.openH = this.closeH + this.body.offsetHeight;
    this.node.style.height = this.closeH + 'px';

    this.title.addEventListener('click', this.clickHandler);
  }

  clickHandler() {
    if (this.opened) {
      this.close();
    } else {
      this.open();
    }
  }

  open() {
    this.node.classList.add('-opened');
    this.node.style.height = this.openH + 'px';
    this.opened = true;
  }

  close() {
    this.node.classList.remove('-opened');
    this.node.style.height = this.closeH + 'px';
    this.opened = false;
  }

}


const items = document.querySelectorAll('.faq');

if (items) {
  items.forEach(i => {
    try {
      new Faq(i);
    } catch (e) {
      console.log(e);
    }
  });
}

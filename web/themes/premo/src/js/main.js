import mainSlider from './main-slider';
import modal from './modal';
import mobileMenu from './mobile-menu';
import tabs from './tabs';
import tabsServices from './tabs-services';
import projectSlider from './project-slider';
import animations from './animations';
import companyContent from './company-content.js';
import clientsSlider from './clients-slider.js';
import faq from './faq';
import videoControls from './video-controls';

// import objectFitImages from 'object-fit-images'
// document.addEventListener('DOMContentLoaded', objectFitImages);




class Tab {
  constructor(el) {
    this.state = {
      index: 0,
    };
    this.refs = {
      el: el,
      buttons: {},
      contents: el.querySelector('.tabs__content'),
    };

    this.setActive = this.setActive.bind(this);
    this.init();
  }

  init() {
    this.indexItems();
    this.setInitActive();

    const buttons = this.refs.buttons;
    for (let btn of Object.values(buttons)) {
      btn.addEventListener('click', (e) => {
        const index = e.target.tabId;
        this.setActive(index);
      });
    }

  }

  indexItems() {
    const el = this.refs.el;

    // buttons
    const buttons = el.querySelectorAll('.tabs__tab');
    buttons.forEach((btn, index) => {
      btn.tabId = index;
      this.refs.buttons[index] = btn;
    });

    // contents
    const contents = el.querySelectorAll('.tabs__content');
    contents.forEach((cnt, index)  => {
      cnt.tabId = index;
      this.refs.contents[index] = cnt;
    });
  }

  setInitActive() {
    const currActive = this.refs.el.querySelector('.tabs__tab.-active');
    let index = 0;

    if (currActive) {
      index = currActive.tabId;
    }

    this.setActive(index);
  }

  setActive(index) {
    const prevIndex = this.state.index;
    const nextIndex = index;

    this.refs.buttons[prevIndex].classList.remove('-active');
    this.refs.contents[prevIndex].classList.remove('-active');

    this.refs.buttons[nextIndex].classList.add('-active');
    this.refs.contents[nextIndex].classList.add('-active');

    this.state.index = nextIndex;

    const payload = {
      index: nextIndex,
      text: this.refs.buttons[nextIndex].innerHTML,
    };
    const event = new CustomEvent('update', {
      detail: payload
    });
    this.refs.el.dispatchEvent(event);
  }

  on(eventName, callback) {
    this.refs.el.addEventListener(eventName, callback);
  }
}


try {
  const tabsEls = document.querySelectorAll('.tabs');
  if (tabsEls) {
    window.Tabs = {};
    tabsEls.forEach((tab, index) => {
      const tabsName = tab.getAttribute('data-tabs-name') || index;
      Tabs[tabsName] = new Tab(tab);
    });
  }
} catch (e) {
  console.log(e);
}





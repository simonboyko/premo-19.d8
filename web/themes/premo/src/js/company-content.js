import CrawlLine from './crawl-line';

const node = document.querySelector('.companyContent__scroll');
if (node) {
  new CrawlLine(node, 'companyContent');
}

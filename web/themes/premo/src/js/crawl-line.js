import tween from 'TweenLite';
import TimelineLite from 'TimelineLite';
// import debounce from 'lodash-es/debounce';
import throttle from 'lodash-es/throttle';

let touchStart;
// const  = document;

class CrawlLine {
  constructor(node, bemClass) {
    this.node = node;
    this.bem = bemClass;
    this.state = {
      width: null,
      offset: null,
      stepPx: 50, // px per second,
      duration: null
    };
    this.tl = null;
    this.winWidth = document.documentElement.clientWidth;
    this.SPEED_SWIPE_COEF = (node.offsetWidth / node.scrollWidth) * 0.0275;
    this.arrowWidth = document.querySelector(`.${bemClass}__arrow`).clientWidth;

    this.arrows = null;
    this.tlArrows = null;

    this.reverse = this.reverse.bind(this);
    this.start = this.start.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);

    this.init();
    this.start();
  }

  init() {
    this.state.width = this.node.offsetWidth;
    this.state.offset = this.getOffset();
    this.state.duration = this.getDuration();

    window.addEventListener('resize', () => this.winWidth = document.documentElement.clientWidth );

    this.initTl();
    this.initArrows();
  }

  initTl() {
    const node = this.node;
    const speed = this.state.duration;
    const offset = this.state.offset;

    this.tl = new TimelineLite({
      onComplete: this.reverse,
      onReverseComplete: this.start,
      paused: true
    });
    this.tl.to(node, speed, {scrollTo: {y: 0, x: offset}, ease: Power0.easeNone});
    this.tl.addLabel('test', 10);

    this.node.addEventListener('mouseover', (e) => {
      this.tl.pause();
      this.node.onmousemove = this.handleMouseMove;
    });
    this.node.addEventListener('mouseleave', (e) => {
      this.tl.resume();
      this.hideArrows();
      this.node.onmousemove = null;
    });

    this.node.addEventListener('touchstart', (e) => {
      this.tl.pause();
      touchStart = e.touches[0].clientX;
    });
    this.node.addEventListener('touchmove', throttle((e) => {
      const touchCurrent = e.touches[0].clientX;
      const diff = touchCurrent - touchStart;
      const curPos = this.tl.totalProgress();

      let newPos = curPos - this.SPEED_SWIPE_COEF * diff;
      if (newPos <= 0) {
        newPos = 0.000001;
      } else if (newPos >= 1) {
        newPos = 0.999999;
      }

      tween.to(this.tl, 0.1, { progress: newPos});
      touchStart = touchCurrent;
    }, 16));
    this.node.addEventListener('touchend', (e) => {
      touchStart = null;
      setTimeout(() => {
        if (!touchStart) this.tl.resume();
      }, 1000);
    });
  }

  initArrows() {
    this.arrows = {
      prev: this.node.parentElement.querySelector(`.${this.bem}__arrow--prev`),
      next: this.node.parentElement.querySelector(`.${this.bem}__arrow--next`),
    };
    this.arrows.prev.addEventListener('click', () => {
      const curPos = this.tl.totalProgress();
      let newPos = curPos - 22/this.state.duration;
      newPos = newPos < 0 ? 0 : newPos;

      tween.to(this.tl, 0.4, { progress: newPos});
    });
    this.arrows.next.addEventListener('click', () => {
      const curPos = this.tl.totalProgress();
      let newPos = curPos + 22/this.state.duration;
      newPos = newPos > 1 ? 1 : newPos;

      tween.to(this.tl, 0.4, { progress: newPos});
    });

    this.tlArrows = new tween([this.arrows.prev, this.arrows.next], 0.4, {opacity: 1, x: 0, ease: Power3.easeOut, paused: true});
  }

  handleMouseMove(e) {
    const x = e.clientX;
    if (x <= this.arrowWidth || x >= (this.winWidth - this.arrowWidth)) {
      this.showArrows();
    } else {
      this.hideArrows();
    }
  }

  showArrows() {
    this.tlArrows.play();
  }

  hideArrows() {
    this.tlArrows.reverse();
  }

  start() {
    this.tl.totalDuration(this.state.duration);
    this.tl.restart();
  }

  reverse() {
    this.tl
      .totalDuration(1)
      .reverse();
  }

  getOffset() {
    const nodeW = this.state.width;
    const scrollWidth = this.node.scrollWidth;

    return scrollWidth - nodeW;
  }

  getDuration() {
    const stepPx = this.state.stepPx;
    const width = this.state.offset;

    return width/stepPx;
  }

}

export default CrawlLine;

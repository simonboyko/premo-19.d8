(function ($, Drupal, drupalSettings) {

  'use strict';

  function throttle(func, ms) {

    let isThrottled = false,
      savedArgs,
      savedThis;

    function wrapper() {

      if (isThrottled) { // (2)
        savedArgs = arguments;
        savedThis = this;
        return;
      }

      func.apply(this, arguments); // (1)

      isThrottled = true;

      setTimeout(function() {
        isThrottled = false; // (3)
        if (savedArgs) {
          wrapper.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
      }, ms);
    }

    return wrapper;
  }


  Drupal.behaviors.scroll_progress = {
    attach: function (context, settings) {

      var node = document.querySelector('.scrollProgress__fill');
      function scrollHandler() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        var scrolled = (winScroll / height) * 100;
        node.style.width = scrolled + "%";
      }

      window.addEventListener('scroll', throttle(scrollHandler, 50));

    }
  };

})(jQuery, Drupal, drupalSettings);

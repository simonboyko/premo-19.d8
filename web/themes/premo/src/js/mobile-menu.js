const d = document;
const init = () => {
  const menu = d.querySelector('.menuModal');

  const open = () => {
    menu.classList.add('-opened');
    d.body.classList.add('noScroll');
  };

  const close = () => {
    menu.classList.remove('-opened');
    d.body.classList.remove('noScroll');
  };

  const btn = d.querySelector('.menuModal__button');
  btn.addEventListener('click', open);

  const closer = d.querySelector('.menuModal__closer');
  closer.addEventListener('click', close);
};

try {
  init();
} catch (e) {}

import { tns } from "../../node_modules/tiny-slider/src/tiny-slider";
import smoothScroll from 'SmoothScroll';

const d = document;
const sliderEls = d.querySelectorAll('.projectSlider__slides');
let sliders = [];

const scroll = new smoothScroll();
const scrollToTop = () => {
  const node = d.querySelector('.b-projects__heading');
  const options = {
    speed: 500
  };
  scroll.animateScroll(node, null, options);
};

const init = () => {
  sliderEls.forEach(i => {
    const block = i.parentElement;
    const prevBtn = block.querySelector('.projectSlider__arrow--prev');
    const nextBtn = block.querySelector('.projectSlider__arrow--next');

    const total = block.querySelector('.projectSlider__total');
    const current = block.querySelector('.projectSlider__current');
    const nav = block.querySelector('.projectSlider__nav');

    const initHandler = (e) => {
      if (e.slideCount == 1) {
        nav.hidden = true;
      }
      total.innerHTML = e.slideCount;
      current.innerHTML = '1';
    };

    const opt = {
      container: i,
      mode: 'gallery',
      nav: false,
      prevButton: prevBtn,
      nextButton: nextBtn,
      loop: false,
      onInit: initHandler
    };
    const slider = tns(opt);
    sliders.push(slider);
    slider.events.on('indexChanged', (e) => {
      current.innerHTML = e.displayIndex;
      scrollToTop();
    });

  });

};

if (sliderEls.length) {
  init();
  console.log(sliders);
}


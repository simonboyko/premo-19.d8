class VideoControls {
  constructor(node) {
    this.node = node;
    this.btn = node.querySelector('.showreel__volume');
    this.video = node.querySelector('.showreel__video');

    this.clickHandler = this.clickHandler.bind(this);

    this.init();
  }

  clickHandler(e) {
    e.preventDefault();
    this.video.muted = !this.video.muted;
    if (this.video.muted) {
      this.btn.classList.remove('showreel__volume--unmuted');
    } else {
      this.btn.classList.add('showreel__volume--unmuted');
    }
  }

  init() {
    this.btn.addEventListener('click', this.clickHandler);
  }

}

const items = document.querySelectorAll('.showreel');
if (items) {
  items.forEach(i=>{
    try {
      new VideoControls(i);
    } catch (e) {
      console.log(e);
    }
  });
}


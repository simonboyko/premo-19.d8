const d = document;

class Modal {
  constructor(el) {
    this.refs = {
      link: el,
      popup: null,
      close: null,
    };
    this.state = {
      active: false,
    };

    this.clickHandler = this.clickHandler.bind(this);

    try {
      this.init();
    } catch (e) {
      console.log(e);
    }
  }

  init() {
    if (NODE_ENV === 'development') {
      console.log('init Modal');
    }

    const link = this.refs.link;
    const popupSelector = link.getAttribute('data-modal');
    const popup = d.querySelector(popupSelector);
    this.refs.popup = d.querySelector(popupSelector);
    this.refs.popup = popup;
    this.refs.close = popup.querySelector('.modal__close');

    this.addListeners();
  }

  addListeners() {
    if (NODE_ENV === 'development') {
      console.log('f: addListeners');
    }

    this.refs.link.addEventListener('click', this.clickHandler);
    this.refs.close.addEventListener('click', this.clickHandler);
  }

  clickHandler(e) {
    if (NODE_ENV === 'development') {
      console.log('f: clickHandler');
    }

    e.preventDefault();

    const active = this.state.active;
    if (active) {
      this.close();
    } else {
      this.open();
    }
  }

  open() {
    if (NODE_ENV === 'development') {
      console.log('f: open');
    }

    this.refs.popup.classList.add('modal--opened');
    d.body.classList.add('noScroll');
    this.state.active = true;
  }

  close() {
    if (NODE_ENV === 'development') {
      console.log('f: close');
    }

    this.refs.popup.classList.remove('modal--opened');
    d.body.classList.remove('noScroll');
    this.state.active = false;
  }

}

export default (() => {

  const links = d.querySelectorAll('[data-modal]');
  links.forEach(i => new Modal(i));

})();

import tween from 'TweenLite';
import TimelineLite from 'TimelineLite';
import waypoints from 'Waypoints';
import {throttle} from './functions';


document.documentElement.classList.add('anim');


const baseFade = { opacity: 1, y: 0, ease: Circ.easeOut};


// about block
const bAboutImgs = () => {
  const img1 = document.querySelector('.b-about__img1');
  const img2 = document.querySelector('.b-about__img2');

  try {
    tween.to(img1, 1, {opacity: 1, y: 0, ease: Expo.easeOut});
    tween.to(img2, 0.8, {opacity: 1, y: 0, ease: Expo.easeOut, delay: 0.2});
    // tween.to(img2, 1, {opacity: 1, y: 0});
  } catch (e) {
    console.log(e);
  }
};
function bAbout() {
  const node = this.element;

  try {
    tween.to(node, 1, { opacity: 1, x: 0, ease: Circ.easeOut, onComplete: bAboutImgs});
  } catch (e) {
    console.log(e);
  }
  this.destroy();
}
const bAboutEl = document.querySelector('.b-about');
if (bAboutEl) {
  new Waypoint({
    element: bAboutEl,
    handler: bAbout,
    offset: '50%'
  });
}


// services block
function bServices() {
  const node = this.element;

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const bServicesEl = document.querySelector('.b-services');
if (bServicesEl) {
  new Waypoint({
    element: bServicesEl,
    handler: bServices,
    offset: '50%'
  });
}



// awards block
// const bAwards
let bAwardsCount = {value: 0};
let bAwardsCountFinish;

function bAwards() {
  const node = this.element;
  const text = node.querySelector('.b-awards__cnt');
  const num = node.querySelector('.b-awards__number');
  const bg = node.querySelector('.b-awards__bg');
  bAwardsCountFinish = Number(num.innerHTML);

  const bAwardsUpdateHandler = () => {
    num.innerHTML = bAwardsCount.value;
  };

  try {
    tween.to(text, 1.5, { opacity: 1, y: 0, ease: Expo.easeOut});
    tween.to(num, 1.3, { opacity: 1, delay: 0.2, ease: Expo.easeOut});
    tween.to(bAwardsCount, 3, { value: bAwardsCountFinish, roundProps: 'value', delay: 0.2, onUpdate: bAwardsUpdateHandler, ease: Expo.easeOut});
    tween.to(bg, 1.5, { backgroundPositionX: '0, 0.63em, 1.19em, 1.74em', delay: 0.8, ease: Expo.easeOut});
  } catch (e) {
    console.log(e);
  }

}
const bAwardsEl = document.querySelector('.b-awards');
if (bAwardsEl) {
  new Waypoint({
    element: bAwardsEl,
    handler: bAwards,
    offset: '40%'
  });
}



// b-awardsList__num
function awardsPageNums() {
  const els = document.querySelectorAll('.b-awardsList__num');

  let counter = {value: 0};
  let value;
  for (let i = 0; i < els.length; i++) {
    const el = els[i];
    const update = () => {
      el.innerHTML = counter.value;
    };
    counter.value = 0;
    value = +el.innerHTML;
    update();

    tween.to(counter, 2 + i, { value: value, roundProps: 'value', delay: 1, onUpdate: update, ease: Expo.easeOut});
  }


}
try {
  awardsPageNums();
} catch (e) {
  console.log(e);
}


// form block
function bFormProject() {
  const node = this.element;
  const fields = node.querySelectorAll('.form__item, .form__button');

  const tl = new TimelineLite();
  try {
    tl.staggerTo(fields, 1.5, { opacity: 1, y: 0, ease: Expo.easeOut}, 0.2);
  } catch (e) {
    console.log(e);
  }

}
const bFormProjectEl = document.querySelector('.b-form--project');
if (bFormProjectEl) {
  new Waypoint({
    element: bFormProjectEl,
    handler: bFormProject,
    offset: '40%'
  });
}



// about awards block
function awardsAbout() {
  const node = this.element;
  const items = node.querySelectorAll('.awardsAbout__item');

  try {
    tween.to(items, 1, { opacity: 1, x: 0, ease: Expo.easeOut});
  } catch (e) {
    console.log(e);
  }
}
const awardsAboutEl = document.querySelector('.awardsAbout');
if (awardsAboutEl) {
  new Waypoint({
    element: awardsAboutEl,
    handler: awardsAbout,
    offset: '10%'
  });
}

function awardsAboutLinksHandler() {
  const node = this.element;
  try {
    tween.to(node, 1, { opacity: 1, y: 0, ease: Expo.easeOut});
  } catch (e) {
    console.log(e);
  }
}
const awardsAboutLinks = document.querySelector('.awardsAbout__links');
if (awardsAboutLinks) {
  new Waypoint({
    element: awardsAboutLinks,
    handler: awardsAboutLinksHandler,
    offset: '100%'
  });
}



// about team block
function bTeamItemsHandler() {
  const node = this.element;
  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const bTeamItemsEls = document.querySelectorAll('.b-team__item');
if (bTeamItemsEls.length) {
  bTeamItemsEls.forEach(i=> {
    new Waypoint({
      element: i,
      handler: bTeamItemsHandler,
      offset: '70%'
    });
  })

}



// about jobs block
function jobsHandler() {
  const node = this.element;
  const text = node.querySelector('.jobs__main');
  const image = node.querySelector('.jobs__image');

  try {
    tween.to(text, 1, baseFade);
    tween.to(image, 1, { opacity: 1, y: 0, x: -27, ease: Expo.easeOut, delay: 0.6});
  } catch (e) {
    console.log(e);
  }
}
const jobsEl = document.querySelector('.jobs');

if (jobsEl) {
  new Waypoint({
    element: jobsEl,
    handler: jobsHandler,
    offset: '40%'
  });
}



// about books block
function booksHandler() {
  const node = this.element;
  const items = node.querySelectorAll('.books__item');

  const tl = new TimelineLite();
  try {
    tl.staggerTo(items, 1, baseFade, 0.1);
  } catch (e) {
    console.log(e);
  }
}
const booksEl = document.querySelector('.books');
if (booksEl) {
  new Waypoint({
    element: booksEl,
    handler: booksHandler,
    offset: '40%'
  });
}




// textBlock--service
function textBlockServiceHandler() {
  const node = this.element;

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const textBlockService = document.querySelector('.textBlock--service');
if (textBlockService) {
  new Waypoint({
    element: textBlockService,
    handler: textBlockServiceHandler,
    offset: '70%'
  });
}



// textBlock--project
function textBlockProjectHandler() {
  const node = this.element;

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const textBlockProjectEls = document.querySelectorAll('.textBlock--project p, .p-article__body p');
if (textBlockProjectEls) {
  textBlockProjectEls.forEach(i=>{
    tween.to(i, 0 , {opacity: 0, y: 50});
    new Waypoint({
      element: i,
      handler: textBlockProjectHandler,
      offset: '80%'
    });
  });
}




// projectGallery
function projectGalleryStart(direction) {
  const coef = 0.7;
  const node = this.element;
  const nodeH = node.offsetHeight;
  const lines = document.querySelectorAll('.projectGallery__row');

  let lastScrollPos = 0;

  const scrollHandler = () => {
    console.log('scroll');

    console.log(node.getBoundingClientRect().top);
    const scrollPos = window.pageYOffset || document.documentElement.scrollTop;
    const diff = (node.getBoundingClientRect().top) * coef;
    console.log(diff);
    tween.to(lines[0], 1, {x: diff - 200});
    tween.to(lines[1], 1, {x: -diff - 200});
    tween.to(lines[2], 1, {x: diff + 400});

    lastScrollPos = scrollPos <= 0 ? 0 : scrollPos;

  };
  window.addEventListener('scroll', throttle(scrollHandler, 16));

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
function projectGalleryStop() {
  console.log(stop);
  window.removeEventListener('scroll', scrollHandler);
}
const projectGallery = document.querySelector('.projectGallery');
let projectGalleryWaypoint;
if (projectGallery) {
  projectGalleryWaypoint = new Waypoint({
    element: projectGallery,
    handler: projectGalleryStart,
    offset: '100%'
  });

  // projectGalleryWaypoint = new Waypoint({
  //   element: projectGallery,
  //   handler: projectGalleryStop,
  //   offset: 'bottom-in-view'
  // });
}




// b-awardsCase
function awardsCaseHandler() {
  const node = this.element;

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const awardsCase = document.querySelector('.b-awardsCase');
if (awardsCase) {
  tween.to(awardsCase, 0, {opacity: 0, y: 300});
  new Waypoint({
    element: awardsCase,
    handler: awardsCaseHandler,
    offset: '70%'
  });
}





// projectOther
function projectOtherHandler() {
  const node = this.element;
  const bg = node.querySelector('.projectOther__bg');

  try {
    tween.to(bg, 1.5, { backgroundPositionX: '0, 0.63em, 1.19em, 1.74em', delay: 0.8, ease: Expo.easeOut});
  } catch (e) {
    console.log(e);
  }
}
const projectOther = document.querySelector('.projectOther');
if (projectOther) {
  new Waypoint({
    element: projectOther,
    handler: projectOtherHandler,
    offset: '70%'
  });
}



// projectTiter
function projectTiterHandler() {
  const node = this.element;
  const img = node.querySelector('.projectTiter__image');
  const main = node.querySelector('.projectTiter__main');


  try {
    tween.to(img, 1, {opacity: 1, x: 0, delay: 0.3});
    tween.to(main, 0.7, {opacity: 1, y: 0, delay: 0.6});
  } catch (e) {
    console.log(e);
  }
}
const projectTiter = document.querySelector('.projectTiter');
if (projectOther) {
  new Waypoint({
    element: projectTiter,
    handler: projectTiterHandler,
    offset: '70%'
  });
}



// b-faq
function faqHandler() {
  const node = this.element;
  const bg = node.querySelector('.b-faq__bg');

  try {
    tween.to(bg, 1.5, { backgroundPositionX: '0, 0.63em, 1.19em, 1.74em', delay: 0.8, ease: Expo.easeOut});
  } catch (e) {
    console.log(e);
  }
}
const faq = document.querySelector('.b-faq');
if (faq) {
  new Waypoint({
    element: faq,
    handler: faqHandler,
    offset: '70%'
  });
}

function faqRowsHandler() {
  const node = this.element;

  try {
    tween.to(node, 1, baseFade);
  } catch (e) {
    console.log(e);
  }
}
const faqRows = document.querySelectorAll('.b-faq__item');
if (faqRows) {
  tween.to(faqRows, 0, {opacity: 0, y: 50});

  faqRows.forEach(i => {
    new Waypoint({
      element: i,
      handler: faqRowsHandler,
      offset: '80%'
    });
  });

}

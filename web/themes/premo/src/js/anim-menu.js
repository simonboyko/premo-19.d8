(function ($, Drupal, drupalSettings) {

  'use strict';

  function throttle(fn, wait) {
    var time = Date.now();
    return function(...args) {
      if ((time + wait - Date.now()) < 0) {
        fn.apply(this, args);
        time = Date.now();
      }
    }
  }

  Drupal.behaviors.anim_menu = {
    attach: function (context, settings) {
      var header = document.querySelector('.header');

      TweenLite.set(header, {backgroundColor: 'rgba(255, 255, 255, 0)'});
      var animationHeader = new TweenLite.to(header, 1, { paddingTop: 20, paddingBottom: 20, backgroundColor: 'rgba(255, 255, 255, 1)', paused: true });

      function hanldeScroll() {
        var y = window.scrollY;
        console.log(y);
        if (y <= 200) {
          var diff = y/220 <= 1 ? y/200 : 1;
          animationHeader.progress(diff);
          if (y > 50) {
            header.classList.add('header--animate');
          } else {
            header.classList.remove('header--animate');
          }
        }
      }

      if (window.scrollY >=20) {
        animationHeader.progress(1);
        header.classList.add('header--animate');
      }
      window.addEventListener('scroll', throttle(hanldeScroll, 16));

    }
  };

})(jQuery, Drupal, drupalSettings);

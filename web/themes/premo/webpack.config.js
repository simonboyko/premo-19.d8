'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  mode: NODE_ENV || 'development',
  entry: __dirname + '/src/js/main.js',
  output: {
    path: __dirname + '/build/js/',
    filename: 'build.js'
  },

  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),

  ],

  resolve: {
    alias: {
      Waypoints: __dirname + "/node_modules/waypoints/lib/noframework.waypoints.js",
      SmoothScroll: __dirname + "/node_modules/smooth-scroll/dist/smooth-scroll.js",
      // Waypoints: __dirname + "/src/js/vendor/noframework.waypoints.js",
    }
  },

  externals: {
    TweenLite: "TweenLite",
    TimelineLite: "TimelineLite"
  },

  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          presets: ['@babel/preset-env'],
          plugins: ['@babel/plugin-transform-runtime']
        }
      }
    }]
  },

  devtool: NODE_ENV == 'development' ? 'eval' : false,
  watch: NODE_ENV == 'development' ? true : false,

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          warnings: false,
          compress: {
            drop_console: true,
            unsafe: true
          }
        }

      })
    ]
  }

};

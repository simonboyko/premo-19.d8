<?php

namespace Drupal\custom_csv_import;

use Drupal\Component\Plugin\PluginBase;

abstract class CustomCSVImportPluginBase extends PluginBase implements CustomCSVImportPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function processItem($data, &$context);

  /**
   * Load for edit or create new entity by unique id from CSV.
   *
   * @param $id
   *   Unique ID in CSV file.
   * @param $entity_type
   *   The entity type id.
   * @param $bundle
   *   The bundle of entity type.
   * @param $field_id
   *   Proxy field`s name of unique id from CSV for edit.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getContentEntityByUid($id, $entity_type, $bundle, $field_id) {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

    $eid = $storage
      ->getQuery()
      ->condition('type', $bundle)
      ->condition($field_id, $id)
      ->execute();

    if (!empty($eid)) {
      $entity = $storage->load($eid[key($eid)]);
    }
    else {
      $entity = $storage->create([
        'type' => $bundle,
        'langcode' => 'en',
        'uid' => 1,
        'status' => 1,
      ]);

      $entity->set($field_id, $id);
    }

    return $entity;
  }

  /**
   * Get Taxonomy term entity ids by string names with delimiter.
   *
   * @param $names
   *   Format: name1|name2|name3.
   * @param $vid
   *   Taxonomy term`s bundle.
   * @param string $delimiter
   *   The delimiter for array.
   *
   * @return array:int|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTermIdsByNames($names, $vid, $delimiter = '|') {
    if ($names) {
      $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $array = explode($delimiter, $names);

      $ids = $storage->getQuery()
        ->condition('vid', $vid)
        ->condition('name', $array, 'IN')
        ->execute();

      return array_values($ids);
    }

    return NULL;
  }

  /**
   * Get Node entity ids by string names with delimiter.
   *
   * @param $names
   *   Format: name1|name2|name3.
   * @param $type
   *   Node`s bundle.
   * @param string $delimiter
   *   The delimiter for array.
   *
   * @return array:int|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getNodeIdsByNames($names, $type, $delimiter = '|') {
    if ($names) {
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $array = explode($delimiter, $names);

      $ids = $storage->getQuery()
        ->condition('type', $type)
        ->condition('title', $array, 'IN')
        ->execute();

      return array_values($ids);
    }

    return NULL;
  }

  /**
   * If file already exist return file`s id, otherwise create this file and return id.
   *
   * @param $name
   *   The file name.
   * @param $directory
   *   Source and Destination directory for copy files.
   *
   * @return int|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getFileIdByName($name, $directory) {
    $storage = \Drupal::entityTypeManager()->getStorage('file');

    $fid = $storage->getQuery()
      ->condition('filename', $name)
      ->execute();

    if ($fid) {
      return $fid[key($fid)];
    }
    elseif ($name) {
      $source_uri = 'public://import_files/' . $directory . '/' . $name;
      $destination = 'public://' . $directory . '/' . $name;
      $uri = file_unmanaged_copy($source_uri, $destination, FILE_EXISTS_REPLACE);

      $file = $storage->create([
        'uid' => 1,
        'filename' => $name,
        'uri' => $uri,
        'status' => 1,
      ]);
      $file->save();

      return $file->id();
    }

    return NULL;
  }

  /**
   * If Media entity already exist return id, otherwise create this entity and return id.
   *
   * @param $parameter
   *   File id or url to remote video.
   * @param $type
   *   The bundle of Media entity.
   * @param $field_name
   *   Machine name of media type fields for add value.
   *
   * @return int|string|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getMediaId($parameter, $type, $field_name) {
    if ($parameter && $type && $field_name) {
      $storage = \Drupal::entityTypeManager()->getStorage('media');

      $id = $storage->getQuery()
        ->condition('bundle', $type)
        ->condition($field_name, $parameter)
        ->execute();

      if ($id) {
        return $id[key($id)];
      }
      else {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $storage->create([
          'uid' => 1,
          'status' => 1,
          'bundle' => $type,
        ]);
        $media->set($field_name, $parameter);
        $media->save();

        return $media->id();
      }
    }

    return NULL;
  }

}

<?php

namespace Drupal\custom_csv_import;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface CustomCSVImportPluginInterface extends PluginInspectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getId();

  /**
   * {@inheritdoc}
   */
  public function getLabel();

  /**
   * Batch process item.
   *
   * @param $data
   *   CSV strings.
   * @param $context
   *   Add Result name and Status message for current data item. Example:
   *   $context['results'][] = $node->id() . ' : ' . $node->label();
   *   $context['message'] = $node->label();
   */
  public function processItem($data, &$context);

}

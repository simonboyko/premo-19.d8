<?php

namespace Drupal\custom_csv_import\Plugin\CustomCSVImport;

use Drupal\Core\Annotation\Translation;
use Drupal\custom_csv_import\Annotation\CustomCSVImport;
use Drupal\custom_csv_import\CustomCSVImportPluginBase;

/**
 * Class TemplateNode
 * @package Drupal\custom_csv_import\Plugin\CustomCSVImport
 *
 * @CustomCSVImport(
 *   id = "template_node_blog",
 *   label = @Translation("Template Node Blog")
 * )
 */
class TemplateNodeBlog extends CustomCSVImportPluginBase {

  /**
   * @param $data
   * @param $context
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function processItem($data, &$context) {
    foreach ($data as $item) {
      list($id, $title, $summary, $body, $main_image_url, $multimedia, $tags, $nid, $path) = $item;

      $node = $this->getContentEntityByUid($id, 'node','blog', 'field_csv_id');
      $node->set('title', $title);
      $node->set('body', [
       'summary' => $summary,
       'value' => $body,
       'format' => 'full_html',
      ]);
      $node->set('field_old_nid', $nid);
      $node->set('field_old_path', $path);
      $node->set('field_tags', $this->getTermIdsByNames($tags, 'tags', ', '));
      $node->set('field_image', $this->getFileIdByName($main_image_url, 'blog'));
      $node->set('field_multimedia', $this->getMultimediaIdsByUrls($multimedia));
      $node->save();

      $context['results'][] = $node->id() . ' : ' . $node->label();
      $context['message'] = $node->label();
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function getFileIdByName($url, $directory) {
    $name = str_replace('https://eventum-premo.ru/sites/default/files/', '', $url);

    return parent::getFileIdByName($name, $directory);
  }

  /**
   * @param $urls
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getMultimediaIdsByUrls($urls) {
    $mids = [];
    $medias = explode('|', $urls);

    if (!$urls) {
      return NULL;
    }

    foreach ($medias as $url) {
      $type_media = $this->getTypeMediaByUrl($url);

      switch ($type_media) {
        case 'image':
          $fid = $this->getFileIdByName($url, 'media/image');
          $mids[] = $this->getMediaId($fid, $type_media, 'field_media_image');
          break;
        case 'video':
          $fid = $this->getFileIdByName($url, 'media/video');
          $mids[] = $this->getMediaId($fid, $type_media, 'field_media_video_file');
          break;
        case 'remote_video':
          $mids[] = $this->getMediaId($url, $type_media, 'field_media_oembed_video');
          break;
      }
    }

    return $mids;
  }

  /**
   * @param $url
   *
   * @return string|null
   */
  private function getTypeMediaByUrl($url) {
    $type = 'image';

    if (empty($url)) {
      $type = NULL;
    }
    elseif (strpos($url, 'vimeo.com') !== FALSE) {
      $type = 'remote_video';
    }
    elseif (strpos($url, 'youtube.com') !== FALSE) {
      $type = 'remote_video';
    }
    elseif (strpos($url, '.mp4') !== FALSE) {
      $type = 'video';
    }

    return $type;
  }

}

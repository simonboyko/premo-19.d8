<?php

namespace Drupal\custom_csv_import\Plugin\CustomCSVImport;

/**
 * Class TemplateNodeProject
 * @package Drupal\custom_csv_import\Plugin\CustomCSVImport
 *
 * @CustomCSVImport(
 *   id = "template_node_project",
 *   label = @Translation("Template Node Project")
 * )
 */
class TemplateNodeProject extends TemplateNodeBlog {

  public function processItem($data, &$context) {
    foreach ($data as $item) {
      list($id, $title, $body, $client, $services, $is_main, $image, $video, $multimedia, $nid, $path) = $item;

      $node = $this->getContentEntityByUid($id, 'node','project', 'field_csv_id');
      $node->set('title', $title);
      $node->set('body', [
        'value' => $body,
        'format' => 'full_html',
      ]);
      $node->set('field_old_nid', $nid);
      $node->set('field_old_path', $path);
      $node->set('field_client', $this->getTermIdsByNames($client, 'client'));
      $node->set('field_image', $this->getFileIdByName($image, 'project'));
      $node->set('field_multimedia', $this->getMultimediaIdsByUrls($multimedia));
      $node->set('field_media_vimeo', $this->getMediaId($video, 'remote_video', 'field_media_oembed_video'));
      $node->set('field_on_front', $this->getCheckboxValueByText($is_main));

      $services = $this->getNodeIdsByNames($services, 'service');
      if ($services) {
        $node->set('field_services', $services);
        $node->set('field_main', $services[0]);
      }

      $node->save();

      $context['results'][] = $node->id() . ' : ' . $node->label();
      $context['message'] = $node->label();
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function getMultimediaIdsByUrls($urls) {
    if (!$urls) {
      return NULL;
    }

    $medias = explode('|', $urls);
    $new_urls = [];

    foreach ($medias as $media) {
      list($name, $url) = explode('&', $media);
      $new_urls[] = $url;
    }

    $urls_string = implode('|', $new_urls);

    return parent::getMultimediaIdsByUrls($urls_string);
  }

  /**
   * @param $text
   *
   * @return int
   */
  protected function getCheckboxValueByText($text) {
    if ($text == 'Да') {
      return 1;
    }

    return 0;
  }

}

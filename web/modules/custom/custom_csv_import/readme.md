# Custom CSV Import

## Установка
1. В папке */modules/custom*: ```git clone git@gitlab.com:clipsitemodules/custom_csv_import.git```
2. В терминале: ```drush en custom_csv_import -y```

## Использование
1. Написать плагин и положить его в папку: ```/modules/custom/custom_csv_import/src/Plugin/CustomCSVImport```
2. Зайти на страницу загрузки импорта: ```/admin/config/development/custom_csv_import```
3. Загрузить файл и нажать "Сохранить".
4. Выбрать плагин и загрузить импорт. 

## Документация

### Для того чтобы загрузить обыкновенное поле можно использовать следующую конструкцию:

```php
$node->title = $title;
```

### Для длинных форматированных полей:

```php
$node->field_text = [
'value' => $text,
'format' => 'full_html',
];  
```

### Для десятичных цифр:

```php
$number = str_replace(',', '.', $number);
$node->field_number = (float)$number;
```

### Для множественных изображений:

```php
if(!empty($images)){
$file_id = [];
$images_array = explode('|', $images);
foreach ($images_array as $name) {
  if (!empty($name)){
		$source_uri = 'public://import/images/' . $name;
		$destination = 'public://images/' . $name;
		$uri = file_unmanaged_copy($source_uri, $destination, FILE_EXISTS_REPLACE);
		$file = File::create([
			'uid' => 1,
			'filename' => $name,
			'uri' => $uri,
			'status' => 1,
		]);
		$file->save();
		$file_id[] = $file->id();
  }
}
$node->field_images = $file_id;
}
```

### Для множественных файлов:

```php
if(!empty($docs)){
	$file_id = [];
	$docs_array = explode('|', $docs);
	foreach ($docs_array as $name) {
		if (!empty($name)){
			$source_uri = 'public://import/docs/' . $name;
			$destination = 'public://docs/' . $name;
			$uri = file_unmanaged_copy($source_uri, $destination, FILE_EXISTS_REPLACE);
			$file = File::create([
			'uid' => 1,
			'filename' => $name,
			'uri' => $uri,
			'status' => 1,
			]);
			$file->save();
			$file_id[] = $file->id();
		}
	}
	$node->field_documents = $file_id;
}
```

### Для параграфов:

```php
$paragraphs_field = [];
$paragraphs_array = explode("|", $paragraph);
foreach($paragraphs_array as $paragraph_item){
$paragraph_item_item = explode("@", $paragraph_item);
$paragraphs_create = Paragraph::create([
	"type"=>"label_value", 
	'langcode' => 'ru',
	'uid' => 1,
	'status' => 1,
]);
$paragraphs_create->field_label = $paragraph_item_item[0];
$paragraphs_create->field_value = $paragraph_item_item[1];
$paragraphs_create->save();
	$paragraphs_field[] = $paragraphs_create;
}
$node->field_paragraphs = $paragraphs_field;
```

Где:

_$paragraph_ - Столбец с контентом в экселе.

_$paragraph_array_ - массив, который разделен символом |. Каждый элемент массива - это один заполенный параграф.

_$paragraph_item_item_ - массив, который разделен символом @. Каждый элемент массива - это поля одного параграфа.

_label_value_ - это машинное имя параграфа, который нужно создать.


### Для NID:

```php
#Узнаем nid
$query = \Drupal::database()->select('node__field_nid', 'n');
$query->addField('n', 'entity_id');
$query->condition('n.field_nid_value', $import_id);
$query->condition('n.bundle', 'machine_name_of_content_type');
$query->range(0, 1);
$system_id = $query->execute()->fetchField();

# Если указан id, значит мы правим ноду, а не создаем.
if (!empty($system_id)) {
	$node = Node::load($system_id);
}
else {
	$node = Node::create([
	  'type' => 'machine_name_of_content_type',
	  'langcode' => 'ru',
	  'uid' => 1,
	  'status' => 1,
	]);
}
$node->field_nid = $import_id;
```

### Для поля с категорией:

```php
#Категория
$query = \Drupal::database()->select('taxonomy_term__field_tid', 't');
$query->addField('t', 'entity_id');
$query->condition('t.field_tid_value', $category_import_id);
$query->condition('t.bundle', 'machine_name_of_taxonomy');
$query->range(0, 1);
$category_system_id = $query->execute()->fetchField();
# Если термин с таким названием нашелся - просто добавляем.
if ($category_system_id) {
  $node->field_category = $category_system_id;
}  
``` 